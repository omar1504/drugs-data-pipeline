from src.reader.drug_reader import drug_reader
from src.reader.publication_reader import publication_reader
from src.graph_generator.generate_drug_pub_graph import drug_mentions_graph
from src.analyzer.journal_analyzer import journal_analyzer
import argparse
import logging
import json
import sys

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    datefmt="%d-%b-%y %H:%M:%S",
    force=True
)
logger = logging.getLogger(__name__)

def read_datalinks():
    """
    That function reads the links of different data sources from the json file datalinks.json
    The json file has the following schema :
    {
        source_content(drugs/pubmed/trials) : {
            data_format1 = [path1,2,3...]
            data_format2 = [path1,2,3...]
            ...
        }
    }
    :param None
    :return: Dict: the paths of each data format
    """
    with open("datalinks.json") as f:
        data = json.load(f)
    return data

def write_json_result(result, filepath):
    '''
    This function writes the dictionnary into a JSON file.
    :param result: the json object
    :param filepath: the name of the output file
    :return: None
    '''
    with open(filepath, 'w', encoding='utf8') as f:
        f.write(json.dumps(result, ensure_ascii=False, indent=4))

def run_pipeline():
    """
    This function executes the entire pipeline 
    :param None
    :return: None
    """
    datalinks = read_datalinks()

    # Create the argument parser
    parser = argparse.ArgumentParser()
    # Add an argument
    parser.add_argument('--output', type=str, default='./output/graph.json', help='Filepath of the output json file')
    parser.add_argument('--drugs', type=str, default=datalinks['drugs'], help='Filepath of the input drugs file')
    parser.add_argument('--pubmed', type=str, default=datalinks['pubmed'], help='Filepath of the input pubmed file')
    parser.add_argument('--trials', type=str, default=datalinks['clinical_trials'], help='Filepath of the input clinical trials file')
    # Parse the argument
    args = parser.parse_args()

    result_filename = args.output

    drugs = drug_reader.read_drugs_from_csv(args.drugs)

    pubmed = publication_reader.read_publication_file(args.pubmed)

    clinical_trials = publication_reader.read_publication_file(args.trials)

    generated_graph = drug_mentions_graph.generate_drug_pub_graph(drugs=drugs, pubmeds=pubmed, clinical_trials=clinical_trials)

    write_json_result(generated_graph, result_filename)
    
    logger.info(f"The journal that mentions the most different drugs is : {journal_analyzer.search_best_journal(result_graph= generated_graph)}")

if __name__ == "__main__":
    run_pipeline()