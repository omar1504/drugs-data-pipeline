import csv
import json
import logging
from abc import ABCMeta, abstractmethod
from src.cleaner.publication_cleaner import publication_cleaner

field_delimiter = ","
logging.basicConfig(level=logging.INFO)

class publicationReader(metaclass=ABCMeta):
    '''
    Transform each row in the csv/json file to a publication object
    '''
    @abstractmethod
    def read_file(self, filepath):
        raise NotImplementedError

class csvPublicationReader(publicationReader):
    '''
    Read pubmed and clinical trials CSV files 
    '''
    def read_file(self, filepath):
        '''
        Transform CSV rows to a list of Publication objects
        :param filepath: path to the csv file
        :return: list of Publication objects
        '''
        validated_rows = []  # Validated rows are with a drug id not empty
        rejected_rows = []   # the rows with empty drug id should be rejected
        with open(filepath, encoding="utf-8", newline='\n') as f:
            input_rows = csv.reader(f, delimiter=field_delimiter)
            next(input_rows)
            
            for row in input_rows:
                cleaned_row = publication_cleaner.check_row(row)
                if cleaned_row is not None:
                    validated_rows.append(cleaned_row)
                else:
                    rejected_rows.append(row)

        return validated_rows

class jsonPublicationReader(publicationReader):
    '''
    Read pubmed and clinical trials JSON files 
    '''

    def read_file(self, filepath):
        '''
        Transform JSON data to a list of Publication objects
        :param filepath: path to the json file
        :return: list of Publication objects
        '''
        validated_rows = [] # Validated rows are with a drug id not empty
        rejected_rows = []  # the rows with empty drug id should be rejected
        with open(filepath, encoding='utf-8') as f:
            data = json.load(f)
            input_rows = [list(item.values()) for item in data]
            
            for row in input_rows:
                cleaned_row = publication_cleaner.check_row(row)
                if cleaned_row is not None:
                    validated_rows.append(cleaned_row)
                else:
                    rejected_rows.append(row)
        return validated_rows

class publicationReader:
    '''
    This class ensures reading the publications from the corresponding files, cleaning the data and transform 
    data to publication objects
    '''

    def read_publication_file(self, filepaths):
        '''
        Based on file extension, we give the right methode to process the file
        :param filepath: The path to the json or csv file
        :return: list of Publication objects
        '''
        for filepath in filepaths:
            if filepath is None:
                return None
            elif filepath.upper().endswith('CSV'):
                return csvPublicationReader().read_file(filepath)
            elif filepath.upper().endswith('JSON'):
                return jsonPublicationReader().read_file(filepath)
            else:
                logging.error(f"Cannot read the file {filepath}")
                raise NotImplementedError

publication_reader = publicationReader()
