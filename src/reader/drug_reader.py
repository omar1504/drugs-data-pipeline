import csv
from src.cleaner.drug_cleaner import drug_cleaner

field_delimiter = ","

class drugReader:
    '''
    this class contains read drugs from csv file
    '''
    def read_drugs_from_csv(self, filepaths):
        '''
        Transform each row in the csv file to a drug object
        :param filepath: The path to the csv file
        :return: List of Drug objects containing the rows data
        '''
        validated_rows = [] # Validated rows are with a drug id not empty
        rejected_rows = []  # the rows with empty drug id should be rejected
        for filepath in filepaths:
            with open(filepath, newline='\n') as f:
                csv_reader = csv.reader(f, delimiter=field_delimiter)
                next(csv_reader)

                for row in csv_reader:
                    cleaned_row = drug_cleaner.check_row(row)
                    if cleaned_row is not None:
                        validated_rows.append(cleaned_row)
                    else:
                        rejected_rows.append(row)

        return validated_rows
drug_reader = drugReader()