class graphGenerator:
    '''
    This class ensures generating a graph which explains the relation between the drugs, pubmed,
    clinical trials and journals.
    '''

    def search_mentions(self, drug_name, pub_list):
        '''
        A drug is mentionned in a publication means it will be present on the title of the publication
        :param drug_name: Name of the drug
        :param pub_list: List of the Publications
        :return: a list which contains two dictionnary. In the first one we found publication grouped by mention date.
        In the second one we found the journals grouped also by mention date
        '''
        publications = {}
        journals = {}
        for pub in pub_list:
            if drug_name.upper() in pub.title.upper():
                if pub.mention_date in publications:
                    publications[pub.mention_date].append(pub.title)
                else:
                    publications[pub.mention_date] = [pub.title]

                if pub.mention_date in journals:
                    journals[pub.mention_date].append(pub.journal)
                else:
                    journals[pub.mention_date] = [pub.journal]

        return [publications, journals]

    def generate_drug_pub_graph(self, drugs, pubmeds, clinical_trials):
        '''
        Generates a dictionnary with the following schema : 
        {
        drug_id : {
                    "pubmed": { // the founded titles are grouped by date and shown in a list in order to have a great visibility about the concerned drug
                                mention_date1: [title1, title2..], 
                                mention_date2: [title3]
                                ... 
                            },
                    "clinical_trials": { // the founded titles are grouped by date and shown in a list in order to have a great visibility about the concerned drug
                                mention_date1: [title1, title2..],
                                mention_date2: [title3]
                                ...
                            },
                    "journal": { // The drug is referenced in the journal by the date 
                                mention_date1: [Journal_name1],
                                mention_date2: [Journal_name1],
                                mention_date3: [Journal_name2],
                                ...
                            }
                }
        }

        :param drugs: list of drugs
        :param pubmeds: list of medical publications
        :param clinical_trials: list of clinical trials
        :return: dictionnary which describe the relation between the drugs, pubmed,
        clinical trials and journals.
        '''
        graph = {}
        for drug in drugs:
            drug_name = drug.name
            drug_info = {
                "pubmed": {},
                "clinical_trials": {},
                "journal": {}
            }

            pubmed_journals = self.search_mentions(drug_name, pubmeds)
            
            drug_info["pubmed"] = pubmed_journals[0]
            drug_info["journal"] = pubmed_journals[1]

            clinical_trials_journals = self.search_mentions(drug_name, clinical_trials)
            drug_info["clinical_trials"] = clinical_trials_journals[0]
            for key in clinical_trials_journals[1]:
                if key in drug_info["journal"]:
                    drug_info["journal"][key].append(clinical_trials_journals[1][key])
                else:
                    drug_info["journal"][key] = clinical_trials_journals[1][key]
                
            drug_info["journal"] = {key:list(set(drug_info["journal"][key])) for key in drug_info["journal"]}  

            graph[drug.id] = drug_info
        return graph

drug_mentions_graph = graphGenerator()