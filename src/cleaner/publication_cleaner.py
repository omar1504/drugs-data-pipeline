from src.obj.publication import publication
from datetime import datetime
import logging
import re

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    datefmt="%d-%b-%y %H:%M:%S",
    force=True
)

logger = logging.getLogger(__name__)
pattern_1 = "%d/%m/%Y"
pattern_2 = "%Y-%m-%d"
pattern_3 = "%d %B %Y"

class publicationCleaner:
    '''
    
    '''
    def match_pattern(self, pubDate, pattern):
        '''
        Check if the pattern is correct for the given date
        :param pubDate: the input date 
        :param pattern: the patern to check
        :return: bool: return True if the pattern matchs the date else False
        '''
        regex = datetime.strptime
        try:
            assert regex(pubDate, pattern)
            return True
        except:
            return False

    def get_datepattern(self, pubDate):
        '''
        Find the right pattern based on the previous method of matching
        :param pubDate: the input date string
        :return: str: the pattern of the input string
        '''
        if self.match_pattern(pubDate, pattern_1):
            return pattern_1
        elif self.match_pattern(pubDate, pattern_2):
            return pattern_2
        elif self.match_pattern(pubDate, pattern_3):
            return pattern_3
        else:
            return None

    def remove_escape_characters(self, field):
        '''
        Replace escaped characters by empty string
        :param field: the input field to check
        :return: str: return a string without escaped characters
        '''
        return re.sub(r'\\x[a-f0-9]{2}', '', str(field).strip())

    def check_row(self, row)-> publication:
        '''
        Check if the publication ID is not empty because it's considered as a primary key
        Remove escape characters 
        Format the mention date
        :param row: the input row
        :return: str: return a publication object if the publication ID is not null else it returns None
        '''
        pubId = self.remove_escape_characters(row[0])
        if pubId != '':
            pubTitle = self.remove_escape_characters(row[1])
            pubDate = self.remove_escape_characters(row[2])
            pubJournal = self.remove_escape_characters(row[3])
    
            pattern = self.get_datepattern(pubDate)
            try:
                # Format date into the international standard date notation which is YYYY-MM-DD
                formatedDate = datetime.strptime(pubDate, pattern).strftime(pattern_2)
                return publication(pubId, pubTitle, formatedDate, pubJournal)
            except:
                logger.error(f"could not convert {pubDate} to datetime!")
                return None
            
        else:
            return None

publication_cleaner = publicationCleaner()