from src.obj.drug import drug
import re 

class drugCleaner:
    '''
        This class contains cleaning methods of drugs   
    '''
    def remove_escape_characters(self, field):
        '''
        Replace escaped characters by empty string
        :param field: the input field to check
        :return: str: return a string without escaped characters
        '''
        return re.sub(r'\\x[a-f0-9]{2}', '', str(field).strip())

    def check_row(self, row)-> drug:
        '''
        Check if the drug ID is not empty because it's considered as a primary key
        :param row: the input row
        :return: str: return a drug object if the drug ID is not null else it returns None
        '''
        drug_id = self.remove_escape_characters(row[0])
        if drug_id != '': 
            drug_name = self.remove_escape_characters(row[1])
            return drug(drug_id, drug_name)
        else:
            return None

drug_cleaner = drugCleaner()