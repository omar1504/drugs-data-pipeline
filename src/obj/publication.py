import datetime

class publication:
    '''
        The publication object can be a pubmed or clinical trials.
        This object is defined by an ID, a title, a journal and a mention_date
    '''
    id:str
    title: str
    journal: str
    mention_date: datetime.datetime

    def __init__(self, id, title, mention_date, journal):
        self.id = id
        self.title = title
        self.mention_date = mention_date
        self.journal = journal