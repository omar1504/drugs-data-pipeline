FROM python:3.8

COPY requirements.txt ./
RUN pip3 install -r requirements.txt

ENV APP_HOME /app
WORKDIR $APP_HOME
COPY . ./

COPY main.py .
ENTRYPOINT ["python",  "main.py"]
CMD