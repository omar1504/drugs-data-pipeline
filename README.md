# PART 1 : Drugs

### Data Pipeline 

The goal of this project is to develop a data pipeline based on drugs data.

The developed pipeline is composed of :

**1. Reading & cleaning of the data source**

It consists of reading data rows from different formats and parse them either into drugs or publication objects.
But before parsing we have implemented some cleaning rules which are :

- removing rows with empty ID values
- normalizing datetime format
- removing escape characters like : \\xc3\\x28

**2. Graph generation**

Based on the graph in description, I loop on drugs objects and I search drug name in pubmed & clinical trials titles. 
The main keys of our output dictionnary are drug_ids. On a second level, we have the related entities to drugs which are Pubmed, clinical_trials and journal. 
In pubmed and clinical_trials, we will found in the JSON the titles of publications referenced by date. Finaly, we will have in the output the associated journals to these publications referenced also by date of mention. 

This vision was translated by the following JSON Schema : 
```yaml
{
        drug_id : {
                    "pubmed": { // the founded titles are grouped by date and shown in a list in order to have a great visibility about the concerned drug
                                mention_date1: [title1, title2..], 
                                mention_date2: [title3]
                                ... 
                            },
                    "clinical_trials": { // the founded titles are grouped by date and shown in a list in order to have a great visibility about the concerned drug
                                mention_date1: [title1, title2..],
                                mention_date2: [title3]
                                ...
                            },
                    "journal": { // The drug is referenced in the journal by the date 
                                mention_date1: [Journal_name1],
                                mention_date2: [Journal_name1],
                                mention_date3: [Journal_name2],
                                ...
                            }
                }
    }
```

**3. Analysis : Search the best journal (Traitement ad-hoc)**

It is based on the output graph and it consists of returning the journal which mentions the most differents drugs.

#### Usage : 
This pipeline can be launched using :
```python
python main.py --help                                                                                              2 ↵
usage: main.py [-h] [--output OUTPUT] [--drugs DRUGS] [--pubmed PUBMED] [--trials TRIALS]

optional arguments:
  -h, --help       show this help message and exit
  --output OUTPUT  Filepath of the output json file
  --drugs DRUGS    Filepath of the input drugs file
  --pubmed PUBMED  Filepath of the input pubmed file
  --trials TRIALS  Filepath of the input clinical trials file
```

I have created a datalinks JSON file which contains path of the four given files :  clinical_trials.csv, drugs.csv, pubmed.csv, pubmed.json.
So by default, if you don't put an argument of a specific file, the script will take by default the input files founded in datalinks.json 

```python
python main.py
```
Output : 
```python
13-Oct-22 17:31:49 - __main__ - INFO - The journal that mentions the most different drugs is : Psychopharmacology
```
# Questions 

- Quels sont les éléments à considérer pour faire évoluer votre code afin qu’il puisse gérer de grosses volumétries de données (fichiers de plusieurs To ou millions de fichiers par exemple) ?

    - Réponse : Afin de gérer de grosses volumétries de données, je propose de passer au calcul distribué en utilisant le framework Apache Spark.

- Pourriez-vous décrire les modifications qu’il faudrait apporter, s’il y en a, pour prendre en considération de telles volumétries ?

    - Réponse : Les modifications qu’il faut apporter sont : 
        - Au lieu de lire le fichier CSV ligne par ligne et le parser en objet drug ou bien publication, on utilise spark.read.csv en précisant le chemin du fichier ainsi que le schéma de la table
        - Pour la lecture du fichier JSON, on utilise spark.read.json
        - Une fois les fichiers sont parsés en PySpark DataFrame, on peut exploiter les méthodes prédefinies telque groupBy(), to_timestamp(), join(), union(), write.format('json')

# PART 2 : SQL 
1. Par rapport à la table TRANSACTION, on suppose que la colonne transaction_date est de type DATE
``` sql
SELECT DATE_FORMAT(transaction_date, "%d/%m/%Y") AS formated_date, SUM(prod_price * prod_qty) AS ventes 
FROM TRANSACTION
WHERE transaction_date BETWEEN '01/01/19' AND '31/12/19'
GROUP BY transaction_date
ORDER BY formated_date ASC;
``` 

2. 
``` sql
SELECT TRANSACTION.client_id,
	SUM(CASE WHEN PRODUCT_NOMENCLATURE.product_type = 'MEUBLE' THEN TRANSACTION.prod_price*TRANSACTION.prod_qty ELSE 0 END) AS ventes_meubles,
	SUM(CASE WHEN PRODUCT_NOMENCLATURE.product_type = 'DECO' THEN TRANSACTION.prod_price*TRANSACTION.prod_qty ELSE 0 END) AS ventes_deco
FROM TRANSACTION
JOIN PRODUCT_NOMENCLATURE ON TRANSACTION.prod_id = PRODUCT_NOMENCLATURE.product_id
WHERE TRANSACTION.transaction_date BETWEEN '01/01/19' AND '31/12/19'
GROUP BY TRANSACTION.client_id;
```
